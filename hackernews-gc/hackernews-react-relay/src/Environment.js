import { GC_AUTH_TOKEN } from './constants'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import { execute } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';

const {
  Environment,
  Network,
  RecordSource,
  Store,
} = require('relay-runtime')

const store = new Store(new RecordSource())

export const fetchQuery = (operation, variables) => {
  return fetch('https://api.graph.cool/relay/v1/cjr9jjoy6ckew01438c95r724', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem(GC_AUTH_TOKEN)}`
    },
    body: JSON.stringify({
      query: operation.text,
      variables,
    }),
  }).then(response => {
    return response.json()
  })
}

// Subscriptions section
// --------------------------------
const subscriptionClient = new SubscriptionClient('wss://subscriptions.graph.cool/v1/cjr9jjoy6ckew01438c95r724', {reconnect: true})

const subscriptionLink = new WebSocketLink(subscriptionClient);

// Prepar network layer from apollo-link for graphql subscriptions
const networkSubscriptions = (operation, variables) =>
  execute(subscriptionLink, {
    query: operation.text,
    variables,
});

// const network = Network.create(fetchQuery, setupSubscription)
const network = Network.create(fetchQuery, networkSubscriptions)

const environment = new Environment({
  network,
  store,
})

export default environment
